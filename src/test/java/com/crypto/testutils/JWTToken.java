package com.crypto.testutils;

import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.Claims;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Map;
import java.util.Set;

/**
 * Utility for generating JWT access tokens
 */
@Slf4j
public class JWTToken {

    public  final static Map<String, String> exchangesURL = Map.of("binance", "binance.agent");
    public final static Set<String> ROOT_ROLES = Set.of("Market Data", "Orders", "Account", "Administration");

    public final static int TOKEN_LIVE_TIME = 10; // in secs
    public final static String PRIVATE_KEY_FILENAME = "/privateKey.pem";

    /**
     * Utility method to generate a JWT string from a JSON resource file that is signed by the privateKey.pem
     * test resource key, possibly with invalid fields.
     */
    public static String newToken(String exchange) {

        long currentTimeInSecs = Utils.currentTimeInSecs();
        long exp = currentTimeInSecs + TOKEN_LIVE_TIME;
        String exchangeURL = exchangesURL.get(exchange);
        PrivateKey privateKey;
        try {
            privateKey = readPrivateKey(PRIVATE_KEY_FILENAME);
        } catch (Exception e ){
            e.printStackTrace();
            return "";
        }

        JwtClaimsBuilder claims = Jwt.claims();
        claims.issuer("https://" + exchangeURL +"/using-jwt-rbac")
                .claim("upn", "lbom@" + exchangeURL)
                .claim("aud", "using-jwt-rbac")
                .claim("nickname", "lbom")
                .groups(ROOT_ROLES)
                .issuedAt(currentTimeInSecs)
                .claim(Claims.auth_time.name(), currentTimeInSecs)
                .expiresAt(exp);

        return claims.jws().signatureKeyId(PRIVATE_KEY_FILENAME).sign(privateKey);
    }

    /**
     * Read a PEM encoded private key from the classpath
     *
     * @param pemResName - key file resource name
     * @return PrivateKey
     * @throws Exception on decode failure
     */
    public static PrivateKey readPrivateKey(final String pemResName) throws Exception {
        try (InputStream contentIS = JWTToken.class.getResourceAsStream(pemResName)) {
            byte[] tmp = new byte[4096];
            int length = contentIS.read(tmp);
            return decodePrivateKey(new String(tmp, 0, length, StandardCharsets.UTF_8));
        }
    }

    /**
     * Decode a PEM encoded private key string to an RSA PrivateKey
     *
     * @param pemEncoded - PEM string for private key
     * @return PrivateKey
     * @throws Exception on decode failure
     */
    public static PrivateKey decodePrivateKey(final String pemEncoded) throws Exception {
        byte[] encodedBytes = toEncodedBytes(pemEncoded);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private static byte[] toEncodedBytes(final String pemEncoded) {
        final String normalizedPem = removeBeginEnd(pemEncoded);
        return Base64.getDecoder().decode(normalizedPem);
    }

    private static String removeBeginEnd(String pem) {
        pem = pem.replaceAll("-----BEGIN (.*)-----", "");
        pem = pem.replaceAll("-----END (.*)----", "");
        pem = pem.replaceAll("\r\n", "");
        pem = pem.replaceAll("\n", "");
        return pem.trim();
    }
}