package com.crypto.websocket;

import com.crypto.Exchange;
import com.crypto.messages.BarDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import java.math.BigDecimal;
import java.net.URI;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

@QuarkusTest
public class BarsStreamTest {

    final CountDownLatch countDownLatch = new CountDownLatch(1);
    final static String url  = "ws://127.0.0.1:8083/";
    final static String channel = "bars/";
    final static String pair = "btcusdt";
    final static String period = "1m";
    final static int barLiveTime = 59_999; //in ms

    @Test
    @Timeout(value = 63, unit = SECONDS)
    public void webSocketBarsDataCorrectness() {

        WebSocketClient socketClient = new ReactorNettyWebSocketClient();
        socketClient.execute(URI.create(url + channel + pair + "/" + period),
                session -> session.receive()
                        .limitRequest(1)
                        .doOnError(this::onError)
                        .map(WebSocketMessage::getPayloadAsText)
                        .doOnNext(this::onEvent)
                        .log()
                        .then()).subscribe();

        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }

    private void onError(Throwable e) {
        Assertions.fail("Error while consuming messages: " + e.getMessage());
        System.exit(-1);
    }

    private void onEvent(String message) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {

            BarDTO dto = objectMapper.readValue(message, BarDTO.class);
            Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
            Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
            Assertions.assertEquals(dto.getPair(), pair);
            Assertions.assertEquals(dto.getPeriod(), period);

            Assertions.assertTrue(dto.getBaseVolume().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getCostVolume().compareTo(BigDecimal.ZERO) > 0);

            Assertions.assertTrue(dto.getBaseBuyVolume().compareTo(BigDecimal.ZERO) >= 0);
            Assertions.assertTrue(dto.getCostBuyVolume().compareTo(BigDecimal.ZERO) >= 0);
            Assertions.assertTrue(dto.getBaseSellVolume().compareTo(BigDecimal.ZERO) >= 0);
            Assertions.assertTrue(dto.getCostSellVolume().compareTo(BigDecimal.ZERO) >= 0);
            Assertions.assertTrue(dto.getCostBuyVolume().compareTo(dto.getBaseBuyVolume()) > 0);
            Assertions.assertTrue(dto.getCostSellVolume().compareTo(dto.getBaseSellVolume()) > 0);

            Assertions.assertTrue(dto.getHigh().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getLow().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getClose().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getOpen().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getHigh().compareTo(dto.getLow()) >= 0);

            Assertions.assertEquals(barLiveTime, dto.getCloseTime() - dto.getOpenTime());
            countDownLatch.countDown();

        } catch (JsonProcessingException e) {
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }
}