package com.crypto.websocket;

import com.crypto.Exchange;
import com.crypto.messages.TradeDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

@QuarkusTest
public class TradesStreamTest {

    WebSocketClient socketClient = new ReactorNettyWebSocketClient();
    CountDownLatch countDownLatch = new CountDownLatch(1);
    ArrayList<TradeDTO> messages = new ArrayList<>();
    final static String url  = "ws://127.0.0.1:8083/";
    final static String channel = "trades/";
    final static String pair = "btcusdt";

    @Test
    @Timeout(value = 10, unit = SECONDS)
    public void webSocketTradesDataCorrectness() {

        socketClient.execute(URI.create(url + channel + pair),
            session -> session.receive()
                    .limitRequest(10)
                    .doOnError(this::onError)
                    .map(WebSocketMessage::getPayloadAsText)
                    .doOnNext(this::onEvent)
                    .log()
                    .doOnComplete(()-> {
                        Assertions.assertTrue(testTradesCorrectness(messages));
                        countDownLatch.countDown();
                    })
                    .then()).subscribe();

        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }

    private void onError(Throwable e) {
        Assertions.fail("Error while consuming messages: " + e.getMessage());
        System.exit(-1);
    }

    private void onEvent(String message) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TradeDTO dto = objectMapper.readValue(message, TradeDTO.class);
            messages.add(dto);
        } catch (JsonProcessingException e) {
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }

    private boolean testTradesCorrectness(ArrayList<TradeDTO> messages){

        long trade_id = 0;
        for (TradeDTO dto : messages) {
            if (trade_id != 0) {
                Assertions.assertEquals(dto.getId() - 1, trade_id);
            }
            Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
            Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
            Assertions.assertEquals(dto.getPair(), pair);
            Assertions.assertTrue(dto.getVolume().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getPrice().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getCostVolume().compareTo(BigDecimal.ZERO) > 0);
            trade_id = dto.getId();
        }
        return true;
    }
}