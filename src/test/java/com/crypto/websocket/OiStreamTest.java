package com.crypto.websocket;

import com.crypto.Exchange;
import com.crypto.messages.OiDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import java.math.BigDecimal;
import java.net.URI;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

@QuarkusTest
public class OiStreamTest {

    final CountDownLatch countDownLatch = new CountDownLatch(1);
    final static String url  = "ws://127.0.0.1:8083/";
    final static String channel = "oi/";
    final static String pair = "btcusdt";

    @Test
    public void webSocketOiDataCorrectness() {

        WebSocketClient socketClient = new ReactorNettyWebSocketClient();
        socketClient.execute(URI.create(url + channel + pair),
                session -> session.receive()
                        .limitRequest(1)
                        .doOnError(this::onError)
                        .map(WebSocketMessage::getPayloadAsText)
                        .doOnNext(this::onEvent)
                        .log()
                        .then()).subscribe();

        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }

    private void onError(Throwable e) {
        Assertions.fail("Error while consuming messages: " + e.getMessage());
        System.exit(-1);
    }

    private void onEvent(String message) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {

            OiDTO dto = objectMapper.readValue(message, OiDTO.class);
            Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
            Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
            Assertions.assertEquals(dto.getPair(), pair);

            Assertions.assertTrue(dto.getSumOpenInterest().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getSumOpenInterestCost().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getSumOpenInterestCost().compareTo(dto.getSumOpenInterest()) > 0);

            countDownLatch.countDown();

        } catch (JsonProcessingException e) {
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }
}