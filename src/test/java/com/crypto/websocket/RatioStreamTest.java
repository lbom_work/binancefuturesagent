package com.crypto.websocket;


import com.crypto.Exchange;
import com.crypto.messages.RatioDTO;
import com.crypto.services.streaming.Streaming;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import java.math.BigDecimal;
import java.net.URI;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

@QuarkusTest
public class RatioStreamTest {

    final CountDownLatch countDownLatch = new CountDownLatch(1);
    final static String url  = "ws://127.0.0.1:8083/";
    final static String channel = "ratio/";
    final static String pair = "btcusdt";

    @Test
    public void webSocketRatioDataCorrectness() {

        WebSocketClient socketClient = new ReactorNettyWebSocketClient();
        socketClient.execute(URI.create(url + channel + pair),
                session -> session.receive()
                        .limitRequest(1)
                        .doOnError(this::onError)
                        .map(WebSocketMessage::getPayloadAsText)
                        .doOnNext(this::onEvent)
                        .log()
                        .then()).subscribe();
        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }

    private void onError(Throwable e) {
        Assertions.fail("Error while consuming messages: " + e.getMessage());
        System.exit(-1);
    }

    private void onEvent(String message) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {

            RatioDTO dto = objectMapper.readValue(message, RatioDTO.class);
            Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
            Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
            Assertions.assertEquals(dto.getPair(), pair);
            Assertions.assertTrue(dto.getBuySum().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(dto.getSellSum().compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertNotEquals(dto.getRatio(), BigDecimal.ZERO);
            Assertions.assertTrue(dto.getType_id() <= Streaming.Ratio.TOP_POSITION.ordinal());

            BigDecimal ratio = dto.getRatio();
            BigDecimal buySum = dto.getBuySum();
            BigDecimal sellSum = dto.getSellSum();

            if (sellSum.compareTo(buySum) > 0) {
                Assertions.assertTrue(ratio.compareTo(BigDecimal.ZERO) < 0);
            } else {
                Assertions.assertTrue(ratio.compareTo(BigDecimal.ZERO) > 0);
            }

            if (ratio.compareTo(BigDecimal.ZERO) > 0) Assertions.assertTrue(sellSum.compareTo(buySum) < 0);
            if (ratio.compareTo(BigDecimal.ZERO) < 0) Assertions.assertTrue(sellSum.compareTo(buySum) > 0);

            Assertions.assertTrue(dto.getType_id() <= Streaming.Ratio.TOP_POSITION.ordinal());

            countDownLatch.countDown();

        } catch (JsonProcessingException e) {
            Assertions.fail(e.getMessage());
            System.exit(-1);
        }
    }
}