package com.crypto.rest;

import com.crypto.Exchange;
import com.crypto.messages.OiDTO;
import com.crypto.testutils.JWTToken;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.mapper.ObjectMapperType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;


@QuarkusTest
public class OiRestTest {

    private String token;
    private String period = "5m";

    @BeforeEach
    public void generateToken() {
        token = JWTToken.newToken("binance");
    }

    @Test
    public void RestOiAuthorization() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(200);
    }

    @Test
    public void RestOiAuthorizationFailing() {

        given()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(401);
    }

    /**
     * If you pass exchange pair that have max depth < Exchange.QUOTES_MAX_DEPTH this test fails
     */
    @Test
    public void RestOiRequestDataCorrectness() {

        String pair = "btcusdt";
        OiDTO[] data = given().auth()
                .oauth2(token)
                .queryParam("pair", pair)
                .queryParam("period", period)
                .when()
                .get("rest/indicators/oi")
                .as(OiDTO[].class, ObjectMapperType.JACKSON_2);

        for (OiDTO dto : data) {
            Assertions.assertTrue(checkOiDTO(dto, pair));
        }
    }

    @Test
    public void RestOiRequestFailing() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcsdt")
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(204);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "")
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .get("rest/indicators/oi")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", (Object) null)
                .queryParam("period", period)
                .get("rest/indicators/oi")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .queryParam("period", (Object) null)
                .get("rest/indicators/oi")
                .then()
                .statusCode(400);
    }


    private boolean checkOiDTO(OiDTO dto, String pair){

        Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
        Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
        Assertions.assertEquals(dto.getPair(), pair);

        Assertions.assertTrue(dto.getSumOpenInterest().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(dto.getSumOpenInterestCost().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(dto.getSumOpenInterestCost().compareTo(dto.getSumOpenInterest()) > 0);

        return true;
    }
}