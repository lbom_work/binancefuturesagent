package com.crypto.rest;

import com.crypto.Exchange;
import com.crypto.messages.RatioDTO;
import com.crypto.services.streaming.Streaming;
import com.crypto.testutils.JWTToken;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.mapper.ObjectMapperType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class RatioRestTest {

    private String token;
    private final static String period = "5m";

    @BeforeEach
    public void generateToken() {
        token = JWTToken.newToken("binance");
    }

    @Test
    public void RestRatioAuthorization() {

        Assertions.assertTrue(testAuthorization("global_ratio"));
        Assertions.assertTrue(testAuthorization("taker_ratio"));
        Assertions.assertTrue(testAuthorization("top_account_ratio"));
        Assertions.assertTrue(testAuthorization("top_position_ratio"));
    }

    @Test
    public void RestRatioAuthorizationFailing() {

        Assertions.assertTrue(testAuthorizationFailing("global_ratio"));
        Assertions.assertTrue(testAuthorizationFailing("taker_ratio"));
        Assertions.assertTrue(testAuthorizationFailing("top_account_ratio"));
        Assertions.assertTrue(testAuthorizationFailing("top_position_ratio"));
    }

    @Test
    public void RestRatioRequestDataCorrectness() {

        Assertions.assertTrue(testRequestDataCorrectness("global_ratio", Streaming.Ratio.GLOBAL));
        Assertions.assertTrue(testRequestDataCorrectness("taker_ratio", Streaming.Ratio.TAKER));
        Assertions.assertTrue(testRequestDataCorrectness("top_account_ratio", Streaming.Ratio.TOP_ACCOUNT));
        Assertions.assertTrue(testRequestDataCorrectness("top_position_ratio", Streaming.Ratio.TOP_POSITION));
    }

    @Test
    public void RestRatioFailing() {

        Assertions.assertTrue(testRatioFailing("global_ratio"));
        Assertions.assertTrue(testRatioFailing("taker_ratio"));
        Assertions.assertTrue(testRatioFailing("top_account_ratio"));
        Assertions.assertTrue(testRatioFailing("top_position_ratio"));
    }


    private boolean checkRatioDTO(RatioDTO dto, String pair, Streaming.Ratio type) {

        Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
        Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
        Assertions.assertEquals(dto.getPair(), pair);

        BigDecimal ratio = dto.getRatio();
        BigDecimal buySum = dto.getBuySum();
        BigDecimal sellSum = dto.getSellSum();

        Assertions.assertNotEquals(ratio, BigDecimal.ZERO);

        if (sellSum.compareTo(buySum) > 0) {
            Assertions.assertTrue(ratio.compareTo(BigDecimal.ZERO) < 0);
        } else {
            Assertions.assertTrue(ratio.compareTo(BigDecimal.ZERO) > 0);
        }

        if (ratio.compareTo(BigDecimal.ZERO) > 0) Assertions.assertTrue(sellSum.compareTo(buySum) < 0);
        if (ratio.compareTo(BigDecimal.ZERO) < 0) Assertions.assertTrue(sellSum.compareTo(buySum) > 0);

        Assertions.assertEquals(type.ordinal(), dto.getType_id());

        return true;
    }

    public boolean testRatioFailing(String uri) {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcsdt")
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(204);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "")
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", (Object) null)
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .queryParam("period", (Object) null)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(400);

        return true;
    }

    public boolean testRequestDataCorrectness(String uri, Streaming.Ratio type) {

        String pair = "btcusdt";
        RatioDTO[] data = given().auth()
                .oauth2(token)
                .queryParam("pair", pair)
                .queryParam("period", period)
                .when()
                .get("rest/indicators/"+uri)
                .as(RatioDTO[].class, ObjectMapperType.JACKSON_2);

        for (RatioDTO dto : data) {
            Assertions.assertTrue(checkRatioDTO(dto, pair, type));
        }

        return true;
     }

    public boolean testAuthorization(String uri) {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(200);
        return true;
    }

    public boolean testAuthorizationFailing(String uri) {

        given()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/" + uri)
                .then()
                .statusCode(401);
        return true;
    }
}