package com.crypto.rest;

import com.crypto.Exchange;
import com.crypto.messages.RateDTO;
import com.crypto.testutils.JWTToken;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.mapper.ObjectMapperType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;

import static io.restassured.RestAssured.given;


@QuarkusTest
public class RateRestTest {

    private String token;
    private String period = "5m";

    @BeforeEach
    public void generateToken() {
        token = JWTToken.newToken("binance");
    }

    @Test
    public void RestRateAuthorization() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/rate")
                .then()
                .statusCode(200);
    }

    @Test
    public void RestRateAuthorizationFailing() {

        given()
                .queryParam("pair", "btcusdt")
                .queryParam("period", period)
                .get("rest/indicators/rate")
                .then()
                .statusCode(401);
    }

    @Test
    public void RestRateRequestDataCorrectness() {

        String pair = "btcusdt";
        RateDTO[] data = given().auth()
                .oauth2(token)
                .queryParam("pair", pair)
                .queryParam("period", period)
                .when()
                .get("rest/indicators/rate")
                .as(RateDTO[].class, ObjectMapperType.JACKSON_2);

        for (RateDTO dto:  data)  {
            Assertions.assertTrue(checkRateDTO(dto, pair));
        }
    }

    @Test
    public void RestRateRequestFailing() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcsdt")
                .get("rest/indicators/rate")
                .then()
                .statusCode(204);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "")
                .get("rest/indicators/rate")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", (Object) null)
                .get("rest/indicators/rate")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .get("rest/indicators/rate")
                .then()
                .statusCode(400);
    }


    private boolean checkRateDTO(RateDTO dto, String pair){

        Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
        Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
        Assertions.assertEquals(dto.getPair(), pair);

        Assertions.assertNotNull(dto.getFundingRate());

        return true;
    }
}