package com.crypto.messages;

import com.binance.client.model.market.CommonLongShortRatio;
import com.binance.client.model.market.TakerLongShortStat;
import io.vertx.core.json.Json;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;

@NoArgsConstructor
public class RatioDTO {

    @Getter @Setter private String pair;
    @Getter @Setter private int type_id;
    @Getter private String exchange;
    @Getter private String market;
    @Getter private BigDecimal buySum;
    @Getter @Setter private BigDecimal ratio;
    @Getter private BigDecimal sellSum;
    @Getter private long timestamp;

    public RatioDTO(CommonLongShortRatio ratio) {
        this.exchange = NAME;
        this.market = MARKET;
        this.pair = ratio.getSymbol().toLowerCase();
        this.buySum = ratio.getLongAccount();
        this.sellSum = ratio.getShortAccount();
        this.ratio = ratio.getLongShortRatio();
        this.timestamp = ratio.getTimestamp();
    }

    public RatioDTO(TakerLongShortStat ratio) {
        this.exchange = NAME;
        this.market = MARKET;
        this.buySum = ratio.getBuyVol();
        this.sellSum = ratio.getSellVol();
        this.ratio = ratio.getBuySellRatio();
        this.timestamp = ratio.getTimestamp();
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}