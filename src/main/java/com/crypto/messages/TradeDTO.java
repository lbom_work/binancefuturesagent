package com.crypto.messages;

import lombok.Builder;
import lombok.Getter;
import io.vertx.core.json.Json;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@NoArgsConstructor
public final class TradeDTO {

    @Getter private String  exchange;
    @Getter private String  market;
    @Getter private String  pair;
    @Getter private long    id;
    @Getter private boolean buy;
    @Getter private BigDecimal price;
    @Getter private BigDecimal volume;
    @Getter private BigDecimal costVolume;
    @Getter private long    timestamp;

    public TradeDTO(String exchange, String market, String pair, long id,
                    boolean buy, BigDecimal price, BigDecimal volume, BigDecimal  costVolume, long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.id = id;
        this.buy = buy;
        this.price = price;
        this.volume = volume;
        this.costVolume = costVolume;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}
