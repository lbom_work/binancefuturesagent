package com.crypto.messages;

import io.vertx.core.json.Json;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@NoArgsConstructor
public final class BarDTO {

    @Getter private String exchange;
    @Getter private String market;
    @Getter private String pair;
    @Getter private String period;
    @Getter private long   openTime;
    @Getter private BigDecimal open;
    @Getter private BigDecimal high;
    @Getter private BigDecimal low;
    @Getter private BigDecimal close;
    @Getter private long   closeTime;
    @Getter private long   tradesCount;

    @Getter private BigDecimal baseVolume;
    @Getter private BigDecimal costVolume;
    @Getter private BigDecimal baseBuyVolume;
    @Getter private BigDecimal baseSellVolume;
    @Getter private BigDecimal costBuyVolume;
    @Getter private BigDecimal costSellVolume;

    public BarDTO(String exchange, String market, String pair, String period, long openTime, BigDecimal open,
                  BigDecimal high, BigDecimal low, BigDecimal close, long closeTime, long tradesCount,
                  BigDecimal baseVolume, BigDecimal costVolume, BigDecimal baseBuyVolume, BigDecimal baseSellVolume,
                  BigDecimal costBuyVolume, BigDecimal costSellVolume) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.period = period;
        this.openTime = openTime;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.closeTime = closeTime;
        this.tradesCount = tradesCount;
        this.baseVolume = baseVolume;
        this.costVolume = costVolume;
        this.baseBuyVolume = baseBuyVolume;
        this.baseSellVolume = baseSellVolume;
        this.costBuyVolume = costBuyVolume;
        this.costSellVolume = costSellVolume;
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}