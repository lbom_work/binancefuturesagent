package com.crypto.messages;

import lombok.Builder;
import lombok.Getter;

import io.vertx.core.json.Json;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashMap;

@Builder
@NoArgsConstructor
public final class QuoteDTO {

    @Getter private String exchange;
    @Getter private String market;
    @Getter private String pair;
    @Getter private HashMap<BigDecimal, BigDecimal> asks;     //  asks: <price, volume>
    @Getter private HashMap<BigDecimal, BigDecimal> bids;     //  bids: <price, volume>
    @Getter private long   timestamp;

    public QuoteDTO(String exchange, String market, String pair,
                    HashMap<BigDecimal, BigDecimal> asks, HashMap<BigDecimal, BigDecimal> bids,
                    long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.asks = asks;
        this.bids = bids;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}