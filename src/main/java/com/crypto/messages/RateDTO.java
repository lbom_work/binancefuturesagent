package com.crypto.messages;

import com.binance.client.model.market.FundingRate;
import io.vertx.core.json.Json;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;

@Builder
@NoArgsConstructor
public final class RateDTO {

    @Getter private String exchange;
    @Getter private String market;
    @Getter private String pair;
    @Getter private BigDecimal fundingRate;
    @Getter private long fundingTime;

    public RateDTO(String exchange, String market, String pair, BigDecimal fundingRate, long fundingTime) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.fundingRate = fundingRate;
        this.fundingTime = fundingTime;
    }

    public RateDTO(FundingRate rate) {
        this.exchange = NAME;
        this.market = MARKET;
        this.pair = rate.getSymbol().toLowerCase();
        this.fundingRate = rate.getFundingRate();
        this.fundingTime = rate.getFundingTime();
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}