package com.crypto.messages;

import com.binance.client.model.market.OpenInterestStat;
import io.vertx.core.json.Json;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;

@Builder
@NoArgsConstructor
public final class OiDTO {

    @Getter private String exchange;
    @Getter private String market;
    @Getter private String pair;
    @Getter private BigDecimal sumOpenInterest;
    @Getter private BigDecimal sumOpenInterestCost;
    @Getter private long timestamp;

    public OiDTO(String exchange, String market, String pair,
                 BigDecimal sumOpenInterest, BigDecimal sumOpenInterestCost, long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.sumOpenInterest = sumOpenInterest;
        this.sumOpenInterestCost = sumOpenInterestCost;
        this.timestamp = timestamp;
    }

    public OiDTO(OpenInterestStat oi) {
        this.exchange = NAME;
        this.market = MARKET;
        this.pair = oi.getSymbol().toLowerCase();
        this.sumOpenInterest = oi.getSumOpenInterest();
        this.sumOpenInterestCost = oi.getSumOpenInterestValue();
        this.timestamp = oi.getTimestamp();
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}