package com.crypto.services.rest;

import com.binance.client.exception.BinanceApiException;
import com.binance.client.model.market.OrderBook;
import com.binance.client.model.market.OrderBookEntry;
import com.crypto.messages.QuoteDTO;
import com.crypto.Exchange;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;
import static com.crypto.Exchange.QUOTES_MAX_DEPTH;

@Slf4j
@Path("/rest/quotes/")
@ApplicationScoped
public class QuotesRest {

    @Inject
    Exchange exchange;

    @GET
    @Path("snap")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response initFutureQuoteBook(@Context SecurityContext ctx, @QueryParam("pair") String pair) {

        if (pair == null || pair.isBlank())
            return Response.status(400).build();
        OrderBook book;
        try {
            book = exchange.getRestClient().getOrderBook(pair.toUpperCase(), QUOTES_MAX_DEPTH);
        }  catch (BinanceApiException e) {
            return Response.status(400).build();
        }
        return Response.ok(createBook(book.getAsks(), book.getBids(), pair, System.currentTimeMillis())).build();
    }


    private String createBook(List<OrderBookEntry> entryAsks, List<OrderBookEntry> entryBids,
                              String pair, long timestamp) {

        var asks = new HashMap<BigDecimal, BigDecimal>();
        var bids = new HashMap<BigDecimal, BigDecimal>();

        entryAsks.forEach(v -> asks.put(v.getPrice(), v.getQty()));
        entryBids.forEach(v -> bids.put(v.getPrice(), v.getQty()));

        return QuoteDTO.builder()
                .exchange(NAME)
                .market(MARKET)
                .pair(pair)
                .asks(asks)
                .bids(bids)
                .timestamp(timestamp)
                .build().toString();
    }
}