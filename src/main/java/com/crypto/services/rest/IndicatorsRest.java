package com.crypto.services.rest;

import com.binance.client.exception.BinanceApiException;
import com.binance.client.model.enums.PeriodType;
import com.binance.client.model.market.*;
import com.crypto.Exchange;
import com.crypto.messages.RatioDTO;
import com.crypto.messages.OiDTO;
import com.crypto.messages.RateDTO;
import com.crypto.services.streaming.Streaming.Ratio;
import io.reactivex.functions.Function5;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Path("/rest/indicators/")
@ApplicationScoped
public class IndicatorsRest {

    @Inject
    Exchange exchange;

    @GET
    @Path("oi")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOIReq(@Context SecurityContext ctx,
                             @QueryParam("pair") String pair, @QueryParam("period") String period,
                             @QueryParam("limit") Integer limit,
                             @QueryParam("startTime") Long startTime,  @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank() || period == null || period.isBlank())
            return Response.status(400).build();

        List<OiDTO> responseData = getOI(pair, period, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    @GET
    @Path("rate")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRateReq(@Context SecurityContext ctx,
                               @QueryParam("pair") String pair, @QueryParam("limit") Integer limit,
                               @QueryParam("startTime") Long startTime,  @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank())
            return Response.status(400).build();

        List<RateDTO> responseData = getRate(pair, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    @GET
    @Path("top_account_ratio")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTopAccountRatioReq(@Context SecurityContext ctx,
                                          @QueryParam("pair") String pair, @QueryParam("period") String period,
                                          @QueryParam("limit") Integer limit,
                                          @QueryParam("startTime") Long startTime, @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank() || period == null || period.isBlank())
            return Response.status(400).build();

        List<RatioDTO> responseData = getTopAccountRatio(pair, period, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    @GET
    @Path("top_position_ratio")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTopPositionRatioReq(@Context SecurityContext ctx,
                                           @QueryParam("pair") String pair, @QueryParam("period") String period,
                                           @QueryParam("limit") Integer limit,
                                           @QueryParam("startTime") Long startTime, @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank() || period == null || period.isBlank())
            return Response.status(400).build();

        List<RatioDTO> responseData = getTopPositionRatio(pair, period, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    @GET
    @Path("global_ratio")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGlobalRatioReq(@Context SecurityContext ctx,
                                      @QueryParam("pair") String pair, @QueryParam("period") String period,
                                      @QueryParam("limit") Integer limit,
                                      @QueryParam("startTime") Long startTime, @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank() || period == null || period.isBlank())
            return Response.status(400).build();

        List<RatioDTO> responseData = getGlobalRatio(pair, period, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    @GET
    @Path("taker_ratio")
    @RolesAllowed({"Market Data"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTakerReq(@Context SecurityContext ctx,
                                @QueryParam("pair") String pair, @QueryParam("period") String period,
                                @QueryParam("limit") Integer limit,
                                @QueryParam("startTime") Long startTime, @QueryParam("endTime") Long endTime) {

        if (pair == null || pair.isBlank() || period == null || period.isBlank())
            return Response.status(400).build();

        List<RatioDTO> responseData = getTakerRatio(pair, period, limit, startTime, endTime);
        if (responseData.isEmpty())
            return Response.noContent().build();

        return Response.ok(responseData).build();
    }

    public List<OiDTO> getOI(String pair, String period, Integer limit, Long startTime, Long endTime) {

        List<OiDTO> responseData = new ArrayList<>();
        try {
            List<OpenInterestStat> exchangeData = exchange.getRestClient().getOpenInterestStat(pair.toUpperCase(),
                    PeriodType.lookup(period), startTime, endTime, limit);
            exchangeData.forEach(value -> responseData.add(new OiDTO(value)));
        }  catch (BinanceApiException e) {
            log.error(e.getMessage());
        }
        return responseData;
    }


    public List<RateDTO> getRate(String pair, Integer limit, Long startTime, Long endTime) {

        List<RateDTO> responseData = new ArrayList<>();
        try {
            List<FundingRate> exchangeData = exchange.getRestClient().getFundingRate(pair.toUpperCase(),
                    startTime, endTime, limit);
            exchangeData.forEach(value -> responseData.add(new RateDTO(value)));
        }  catch (BinanceApiException e) {
            log.error(e.getMessage());
        }

        return responseData;
    }

    public  List<RatioDTO> getTakerRatio(String pair, String period, Integer limit, Long startTime, Long endTime) {

        List<RatioDTO> responseData = new ArrayList<>();

        try {

            List<TakerLongShortStat> exchangeData = exchange.getRestClient().getTakerLongShortRatio(pair.toUpperCase(),
                    PeriodType.lookup(period), startTime, endTime, limit);

            exchangeData.forEach(value -> {

                RatioDTO dto = new RatioDTO(value);
                dto.setRatio(updateRatio(dto));
                dto.setType_id(Ratio.TAKER.ordinal());
                dto.setPair(pair.toLowerCase());
                responseData.add(dto);
            });
        } catch (BinanceApiException e) {
            log.error(e.getMessage());
        }
        return responseData;
    }

    public List<RatioDTO> getTopAccountRatio(String pair, String period, Integer limit, Long startTime, Long endTime) {
        return getRatio(pair, period, limit, startTime, endTime, Ratio.TOP_ACCOUNT,
                exchange.getRestClient()::getTopTraderAccountRatio);
    }

    public List<RatioDTO> getTopPositionRatio(String pair, String period, Integer limit, Long startTime, Long endTime) {
        return getRatio(pair, period, limit, startTime, endTime, Ratio.TOP_POSITION,
                exchange.getRestClient()::getTopTraderPositionRatio);
    }

    public List<RatioDTO> getGlobalRatio(String pair, String period, Integer limit, Long startTime, Long endTime) {
        return getRatio(pair, period, limit, startTime, endTime, Ratio.GLOBAL,
                exchange.getRestClient()::getGlobalAccountRatio);
    }

    public List<RatioDTO> getRatio(String pair, String period, Integer limit,
                                   Long startTime, Long endTime, Ratio type,
                                   Function5<String, PeriodType, Long, Long, Integer, List<CommonLongShortRatio>> request) {

        List<RatioDTO> responseData = new ArrayList<>();

        try {

            List<CommonLongShortRatio> exchangeData = request.apply(pair.toUpperCase(),
                    PeriodType.lookup(period), startTime, endTime, limit);

            exchangeData.forEach(value -> {

                RatioDTO dto = new RatioDTO(value);
                dto.setRatio(updateRatio(dto));
                dto.setType_id(type.ordinal());

                dto.setPair(pair.toLowerCase());
                responseData.add(dto);
            });
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return responseData;
    }

    private BigDecimal updateRatio(RatioDTO dto) {

        BigDecimal ratio = dto.getRatio();
        BigDecimal buySum = dto.getBuySum();
        BigDecimal sellSum = dto.getSellSum();

        return (sellSum.compareTo(buySum) > 0) ? ratio.negate() : ratio;
    }
}