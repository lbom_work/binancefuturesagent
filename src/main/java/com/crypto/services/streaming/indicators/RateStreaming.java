package com.crypto.services.streaming.indicators;

import com.crypto.messages.RateDTO;
import com.crypto.services.rest.IndicatorsRest;
import com.crypto.services.streaming.Streaming;
import io.quarkus.scheduler.Scheduled;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.crypto.services.streaming.Streaming.RATE_UPDATES_PERIOD;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/rate/{pair}")
public class RateStreaming {

    @Inject
    IndicatorsRest rest;

    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList<>();
    long lastMessageTime = 0L;

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        channels.add(pair);
        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        createMessage();
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }

    @Scheduled(every = RATE_UPDATES_PERIOD)
    public void createMessage() {

        channels.forEach(pair -> {
            List<RateDTO> list = rest.getRate(pair, 1, null, null);
            Optional<Long> time = Optional.of(list.get(0).getFundingTime());
                time.filter(ts -> ts > lastMessageTime)
                    .ifPresent(val -> {
                            lastMessageTime = val;
                            Streaming.sendMessage(pair, list.get(0).toString(), sessions, log);
                        }
                    );
        });
    }
}