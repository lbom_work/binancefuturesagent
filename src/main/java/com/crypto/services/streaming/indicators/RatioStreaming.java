package com.crypto.services.streaming.indicators;

import com.crypto.messages.RatioDTO;
import com.crypto.services.rest.IndicatorsRest;
import com.crypto.services.streaming.Streaming;
import com.crypto.services.streaming.Streaming.Ratio;
import io.quarkus.scheduler.Scheduled;
import io.smallrye.mutiny.tuples.Functions.Function5;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.crypto.services.streaming.Streaming.*;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/ratio/{pair}")
public class RatioStreaming {

    @Inject
    IndicatorsRest rest;

    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList<>();
    ConcurrentHashMap<Ratio, Long> lastMessageTime = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        channels.add(pair);
        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);

        lastMessageTime.put(Ratio.GLOBAL, 0L);
        lastMessageTime.put(Ratio.TAKER, 0L);
        lastMessageTime.put(Ratio.TOP_POSITION, 0L);
        lastMessageTime.put(Ratio.TOP_ACCOUNT, 0L);
        createMessage();
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {
        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }

    @Scheduled(every = RATIO_UPDATES_PERIOD)
    public void createMessage() {

        channels.forEach(pair -> {
            sendMessage(Ratio.GLOBAL, pair, rest::getGlobalRatio);
            sendMessage(Ratio.TAKER, pair, rest::getTakerRatio);
            sendMessage(Ratio.TOP_ACCOUNT, pair, rest::getTopAccountRatio);
            sendMessage(Ratio.TOP_POSITION, pair, rest::getTopPositionRatio);
        });
    }

    public void sendMessage(Ratio ratioType, String pair,
                            Function5<String, String, Integer, Long, Long, List<RatioDTO>> request) {

        List<RatioDTO> list = request.apply(pair, EXCHANGE_PERIOD, 1, null, null);
        Optional<Long> time = Optional.of(list.get(0).getTimestamp());
        time.filter(ts -> ts > lastMessageTime.get(ratioType))
                .ifPresent(val -> {
                        lastMessageTime.put(ratioType, val);
                        list.forEach(dto -> dto.setType_id(ratioType.ordinal()));
                        Streaming.sendMessage(pair, list.get(0).toString(), sessions, log);
                });
    }
}