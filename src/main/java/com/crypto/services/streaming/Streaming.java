package com.crypto.services.streaming;

import org.slf4j.Logger;
import javax.websocket.Session;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class Streaming {

    public static final String EXCHANGE_PERIOD = "5m"; // in seconds
    public static final String OI_UPDATES_PERIOD = "60s"; // in seconds
    public static final String RATE_UPDATES_PERIOD = "80s"; // in seconds
    public static final String RATIO_UPDATES_PERIOD = "100s"; // in seconds
    public static final int QUOTE_ACCUMULATION_TIME = 500; // 0, 100, 250, 500 in milliseconds

    public enum Ratio {
        GLOBAL,
        TAKER,
        TOP_ACCOUNT,
        TOP_POSITION
    }

    public static void sendMessage(String key, String message, Map<String, Session> sessions, Logger log) {

        for (Session s : sessions.values()) {
            if (s.getUserProperties().containsKey(key)) {
                s.getAsyncRemote().sendText(message, result -> {
                    if (!result.isOK()) {
                        log.error(s.getId() + ":" + result.getException().getMessage());
                        s.getUserProperties().remove(key);
                    }
                });
            }
        }
    }

    public static void checkChannels(String key, Map<String, Session> sessions, CopyOnWriteArrayList<String> channels) {

        boolean isAnySubscribed = sessions.values().stream()
                            .map(Session::getUserProperties)
                            .anyMatch(prop -> prop.containsKey(key));
        if (!isAnySubscribed) channels.remove(key);

        sessions.values().removeIf(value -> value.getUserProperties().isEmpty());
    }
}