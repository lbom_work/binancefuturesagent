package com.crypto.services.streaming.basic;

import com.binance.client.exception.BinanceApiException;
import com.binance.client.model.event.OrderBookEvent;
import com.crypto.Exchange;
import com.crypto.messages.QuoteDTO;
import com.crypto.services.streaming.Streaming;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;
import static com.crypto.services.streaming.Streaming.QUOTE_ACCUMULATION_TIME;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/quotes/{pair}")
public class QuotesStreaming {

    @Inject
    Exchange exchange;

    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        if (!channels.contains(pair)) {
            exchange.getWebSocketClient()
                    .subscribeDiffDepthEvent(pair, QUOTE_ACCUMULATION_TIME, this::onExchMessage, this::onExchError);
            channels.add(pair);
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {
        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }

    private void onExchMessage(OrderBookEvent event) {

        String pair = event.getSymbol().toLowerCase();
        var asks = new HashMap<BigDecimal, BigDecimal>();
        var bids = new HashMap<BigDecimal, BigDecimal>();

        event.getAsks().forEach(v -> asks.put(v.getPrice(), v.getQty()));
        event.getBids().forEach(v -> bids.put(v.getPrice(), v.getQty()));

        String message = QuoteDTO.builder()
                .exchange(NAME)
                .market(MARKET)
                .pair(pair)
                .asks(asks)
                .bids(bids)
                .timestamp(event.getEventTime())
                .build().toString();

        Streaming.sendMessage(pair, message, sessions, log);
    }

    public void onExchError(BinanceApiException e) {
        log.info(e.getMessage());
    }
}