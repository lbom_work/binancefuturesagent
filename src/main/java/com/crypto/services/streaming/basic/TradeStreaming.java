package com.crypto.services.streaming.basic;

import com.binance.client.exception.BinanceApiException;
import com.binance.client.model.event.AggregateTradeEvent;
import com.crypto.Exchange;
import com.crypto.messages.TradeDTO;
import com.crypto.services.streaming.Streaming;
import lombok.extern.slf4j.Slf4j;

import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import static com.crypto.Exchange.MARKET;
import static com.crypto.Exchange.NAME;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/trades/{pair}")
public class TradeStreaming {

    @Inject
    Exchange exchange;

    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {
        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        if (!channels.contains(pair)) {
                exchange.getWebSocketClient()
                        .subscribeAggregateTradeEvent(pair, this::onExchMessage, this::onExchError);
                channels.add(pair);
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    private void onExchMessage(AggregateTradeEvent event) {

        String pair = event.getSymbol().toLowerCase();
        var price = event.getPrice();
        var volume = event.getQty();
        var costVolume = price.multiply(volume, new MathContext(8, RoundingMode.HALF_DOWN));
        String message = TradeDTO.builder()
                .exchange(NAME)
                .market(MARKET)
                .pair(pair)
                .id(event.getId())
                .buy(event.getIsBuyerMaker())
                .price(price)
                .volume(volume)
                .costVolume(costVolume)
                .timestamp(event.getEventTime())
                .build().toString();

        Streaming.sendMessage(pair, message, sessions, log);
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }

    public void onExchError(BinanceApiException e) {
        log.info(e.getMessage());
    }
}