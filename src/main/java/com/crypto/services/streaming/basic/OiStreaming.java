package com.crypto.services.streaming.basic;

import com.crypto.messages.OiDTO;
import com.crypto.services.rest.IndicatorsRest;
import com.crypto.services.streaming.Streaming;
import io.quarkus.scheduler.Scheduled;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.crypto.services.streaming.Streaming.*;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/oi/{pair}")
public class OiStreaming {

    @Inject
    IndicatorsRest rest;

    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList<>();
    long lastMessageTime = 0L;

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        channels.add(pair);
        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        createMessage();
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }

    @Scheduled(every = OI_UPDATES_PERIOD)
    public void createMessage() {

        channels.forEach(pair -> {
            List<OiDTO> list = rest.getOI(pair, EXCHANGE_PERIOD, 1, null, null);
            Optional<Long> time = Optional.of(list.get(0).getTimestamp());
            time.filter(ts -> ts > lastMessageTime)
                    .ifPresent(val -> {
                                lastMessageTime = val;
                                Streaming.sendMessage(pair, list.get(0).toString(), sessions, log);
                            });
        });
    }
}