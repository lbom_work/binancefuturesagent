package com.crypto.services.streaming.basic;

import com.binance.client.exception.BinanceApiException;
import com.binance.client.model.enums.CandlestickInterval;
import com.binance.client.model.event.CandlestickEvent;
import com.crypto.Exchange;
import com.crypto.messages.BarDTO;
import com.crypto.services.streaming.Streaming;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/bars/{pair}/{interval}")
public class BarStreaming {

    @Inject
    Exchange exchange;

    CopyOnWriteArrayList<String> channels = new CopyOnWriteArrayList <>();
    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair, @PathParam("interval") String interval) {

        String key  = pair + interval;
        session.getUserProperties().put(key, interval);
        sessions.put(session.getId(), session);
        if (!channels.contains(pair)) {
                exchange.getWebSocketClient().subscribeCandlestickEvent(pair,
                        CandlestickInterval.valueOfLabel(interval), this::onExchMessage, this::onExchError);
                channels.add(pair);
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair, @PathParam("interval") String interval) {

        String key  = pair + interval;
        session.getUserProperties().remove(key);
        Streaming.checkChannels(key, sessions, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error: {}", result.getException().getMessage());
            }
        });
    }

    private void onExchMessage(CandlestickEvent event) {

        if (!event.getIsClosed()) return;

        String pair = event.getSymbol().toLowerCase();
        String key = pair + event.getInterval();

        var volume = event.getVolume();
        var buyVolume =event.getTakerBuyBaseAssetVolume();
        var sellVolume = volume.subtract(buyVolume);

        var costVolume = event.getQuoteAssetVolume();
        var costBuyVolume = event.getTakerBuyQuoteAssetVolume();
        var costSellVolume = costVolume.subtract(costBuyVolume);

        String message = BarDTO.builder()
                .exchange(Exchange.NAME)
                .market(Exchange.MARKET)
                .pair(event.getSymbol().toLowerCase())
                .period(event.getInterval())
                .baseVolume(event.getVolume())
                .costVolume(costVolume)
                .baseBuyVolume(buyVolume)
                .costBuyVolume(costBuyVolume)
                .baseSellVolume(sellVolume)
                .costSellVolume(costSellVolume)
                .openTime(event.getStartTime())
                .closeTime(event.getCloseTime())
                .open(event.getOpen())
                .close(event.getClose())
                .high(event.getHigh())
                .low(event.getLow())
                .tradesCount(event.getNumTrades())
                .build().toString();

        Streaming.sendMessage(key, message, sessions, log);
    }

    public void onExchError(BinanceApiException e) {
        log.info(e.getMessage());
    }
}