package com.crypto;

import com.binance.client.SubscriptionClient;
import com.binance.client.SyncRequestClient;
import lombok.Getter;

import javax.inject.Singleton;
import java.beans.JavaBean;

@JavaBean
@Singleton
public class Exchange {

    public static final String NAME = "binance";
    public static final String MARKET = "futures";
    private static final String API_KEY = "key";
    private static final String SECRET_KEY = "key";
    public static final int QUOTES_MAX_DEPTH = 1000; // max in futures

    @Getter
    private final SubscriptionClient webSocketClient;
    @Getter
    private final SyncRequestClient restClient;

    public Exchange() {
        webSocketClient = SubscriptionClient.create(API_KEY, SECRET_KEY);
        restClient = SyncRequestClient.create(API_KEY, SECRET_KEY);
    }
}